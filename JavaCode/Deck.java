package JavaCode;

import java.util.ArrayList;

public class Deck {
    private ArrayList<Uno> deckOfCards = new ArrayList<Uno>();;

    public Deck(){
        int countWild = 0;
        while (countWild<4){
            WildCard wild = new WildCard("Black");
            this.deckOfCards.add(wild);

            WildPick4Card wild4 = new WildPick4Card("Black");
            this.deckOfCards.add(wild4);

            countWild++;
        }

        String[] colorsArray = {"red", "yellow", "green", "blue"};
        for (String color : colorsArray) {

            int countPerColor = 0;
            while (countPerColor<2){

                for (int i=0; i<=9; i++){
                    NumberedCards num = new NumberedCards(i, color);
                    this.deckOfCards.add(num);      
                }
                SkipCard skip = new SkipCard(color);
                this.deckOfCards.add(skip);  

                ReverseCard reverse = new ReverseCard(color);
                this.deckOfCards.add(reverse);  

                Pick2Card pick2 = new Pick2Card(color);
                this.deckOfCards.add(pick2);  

                countPerColor++;
            }
        }
    }

    public void addToDeck(Uno newCard){
        this.deckOfCards.add(newCard);  
    }

    public Uno draw(){
        Uno firstCard = this.deckOfCards.get(0);
        this.deckOfCards.remove(0);  
        return firstCard;
    }

    public ArrayList<Uno> getDeckOfCards(){
        return this.deckOfCards;
    }
}
