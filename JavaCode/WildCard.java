package JavaCode;

public class WildCard implements Uno{
    String color;

    public WildCard(String color){
        this.color = color;
    }
    public void setColor(String color){
        if (color.equals("red") || color.equals("green") || color.equals("blue") || color.equals("yellow")){ 
        this.color = color;
        }   
    }

    public String getColor(){
        return this.color;
    }
    
    public boolean canPlay(Uno card){     
             return true;
      
    }
}


