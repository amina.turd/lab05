package JavaCode;

public class ReverseCard extends SpecialEffectCard {

    public ReverseCard(String color){
        super(color);
        this.color=color;
        this.specialEffect = "The play will now swap directiong";
    }

    public String getSpecialEffect(){
        return this.specialEffect;
    }

    public boolean canPlay(Uno card){
        if(card instanceof ReverseCard) {
            if(this.specialEffect == ((ReverseCard)card).getSpecialEffect()){
                return true;
            }
        }
        if (this.color.equals(((ColoredCards)card).getColor())){
                return true;
        }
        return false;
    }
}