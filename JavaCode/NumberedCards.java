package JavaCode;

public class NumberedCards extends ColoredCards{
    protected int number;

    public NumberedCards(int number, String color){
        super(color);
        if (number >= 0 && number <=9){ 
            this.number = number;
            this.color = color;
        }
    }

    public int getNumber(){
        return this.number;
    }
    
    public boolean canPlay(Uno card){
        if(card instanceof NumberedCards) {
            if(this.number == ((NumberedCards)card).getNumber()){
                return true;
            }
        }
        if (this.color.equals(((ColoredCards)card).getColor())){
                return true;
        }
        return false;
    }
}

