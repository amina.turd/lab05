package JavaCode;

public class WildPick4Card extends WildCard{   
protected String specialEffect;

    public WildPick4Card(String color){
        super(color);
        this.specialEffect = "Next plyer looses their turn, plus picks 2 cards";
    }

    public String getEffect(){
        String effect="Next plyer looses their turn, plus picks 2 cards";
        return effect;
    }
}


