package JavaCode;

public abstract class SpecialEffectCard extends ColoredCards{

    protected String specialEffect;

    public SpecialEffectCard(String color){
        super(color);
    }

    public abstract String getSpecialEffect();
    
}

