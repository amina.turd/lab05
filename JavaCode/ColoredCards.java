package JavaCode;

public abstract class ColoredCards implements Uno{

    protected String color;

    public ColoredCards(String color){
        if (color.equals("red") || color.equals("green") || color.equals("blue") || color.equals("yellow")){ 
            this.color = color;
        }   
    }

    public String getColor(){
        return this.color;
    }
}

