package JavaCode;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestUno {
    @Test 
    //checking if get method returns the right amount of cards
    public void checkDeckSize(){
        Deck deckUno = new Deck();
        assertEquals(112,deckUno.getDeckOfCards().size());
    }
    
    @Test 
    //checking if addToDeck method works and adds an extra card
    public void checkAddToDeck(){
        Deck deckUno = new Deck();

        WildCard wild = new WildCard("Black");

        deckUno.addToDeck(wild);

        assertEquals(113,deckUno.getDeckOfCards().size());
    }

    @Test 
    //checking if draw method works and deletes a card
    public void checkDraw(){
        Deck deckUno = new Deck();

        deckUno.draw();

        assertEquals(111,deckUno.getDeckOfCards().size());
    }
    
    @Test 
    //checking if can play method works on a card that has same number
    public void checkCanPlayNum(){
        NumberedCards card1 = new NumberedCards(3, "yellow");
        NumberedCards card2 = new NumberedCards(3, "blue");

        assertEquals(true,card1.canPlay(card2));
    }

    @Test 
     //checking if can play method works on a card that has same color
    public void checkCanPlayColor(){
        NumberedCards card1 = new NumberedCards(3, "yellow");
        NumberedCards card2 = new NumberedCards(1, "yellow");

        assertEquals(true,card1.canPlay(card2));
    }

    @Test 
    //checking if can play method does not work on a numbered card that does not share the color not the number
   public void checkCanPlayFail(){
       NumberedCards card1 = new NumberedCards(3, "yellow");
       NumberedCards card2 = new NumberedCards(1, "red");

       assertEquals(false,card1.canPlay(card2));
   }

   @Test 
   //checking if can play method works on a special card
  public void checkCanPlayPick2(){
      Pick2Card card1 = new Pick2Card("yellow");
      Pick2Card card2 = new Pick2Card("red");

      assertEquals(true,card1.canPlay(card2));
  }

  @Test 
  //checking if can play method fails if cards dont have the same special effect
 public void checkCanPlayFailsSpecial(){
     Pick2Card card1 = new Pick2Card("yellow");
     ReverseCard card2 = new ReverseCard("red");

     assertEquals(false,card1.canPlay(card2));
 }

 @Test 
 //checking if can play method passes if cards dont have the same special effect but have same color
public void checkCanPlaySpecial(){
    Pick2Card card1 = new Pick2Card("yellow");
    ReverseCard card2 = new ReverseCard("yellow");

    assertEquals(true,card1.canPlay(card2));
}

@Test 
//checking if can play method passes for a wild card on any other card
public void checkCanPlayWild(){
   WildCard card1 = new WildCard("yellow");
   ReverseCard card2 = new ReverseCard("blue");

   assertEquals(true,card1.canPlay(card2));
}

@Test 
//checking if can play method passes for a wild4 card on any other card
public void checkCanPlayWild4(){
   WildPick4Card card1 = new WildPick4Card("yellow");
   ReverseCard card2 = new ReverseCard("blue");

   assertEquals(true,card1.canPlay(card2));
}

@Test 
//checking if getColor method works on a numbered card
public void checkGetColor(){
   NumberedCards card = new  NumberedCards(2,"blue");

   assertEquals("blue",card.getColor());
}


@Test 
//checking if getNumber method works on a numbered card
public void checkGetNumber(){
   NumberedCards card = new  NumberedCards(2,"blue");

   assertEquals(2,card.getNumber());
}

@Test 
//checking if getColor method works on a numbered card
public void checkGetSpecialEffect(){
   SkipCard card = new  SkipCard("blue");

   assertEquals("Next player looses their turn",card.getSpecialEffect());
}

@Test 
//checking if setColor method works on a wild card
public void checkSetColor(){
   WildCard card = new  WildCard("black");
   card.setColor("red");

   assertEquals("red",card.getColor());
}
@Test 
//checking if setColor method fails on a wild card if the wrong color is set
public void checkSetColorFail(){
   WildCard card = new  WildCard("black");
   card.setColor("purple");

   assertEquals("black",card.getColor());
}
@Test 
//checking if setColor method works on a wild4 card
public void checkSetColorWild4(){
   WildPick4Card card = new  WildPick4Card("black");
   card.setColor("green");
   assertEquals("green",card.getColor());
}
@Test 
//checking if getEffect method works on a wild4 card
public void checkGetEffectWild4(){
   WildPick4Card card = new  WildPick4Card("black");
   assertEquals("Next plyer looses their turn, plus picks 2 cards",card.getEffect());
}
}

