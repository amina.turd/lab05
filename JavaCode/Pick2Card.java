package JavaCode;

public class Pick2Card extends SpecialEffectCard{
  
    public Pick2Card(String color){
        super(color);
        this.color=color;
        this.specialEffect = "Next player looses their turn, plus picks up 2 cards";

    }

    public String getSpecialEffect(){
        return this.specialEffect;
    }

    public boolean canPlay(Uno card){
        if(card instanceof Pick2Card) {
            if(this.specialEffect == ((Pick2Card)card).getSpecialEffect()){
                return true;
            }
        }
        if (this.color.equals(((ColoredCards)card).getColor())){
                return true;
        }
        return false;
    }
}