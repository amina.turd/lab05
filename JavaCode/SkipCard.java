package JavaCode;

public class SkipCard extends SpecialEffectCard {

    public SkipCard(String color){
        super(color);
        this.specialEffect = "Next player looses their turn";
    }

    public String getSpecialEffect(){
        return this.specialEffect;
    }

    public boolean canPlay(Uno card){
        if(card instanceof SkipCard) {
            if(this.specialEffect == ((SkipCard)card).getSpecialEffect()){
                return true;
            }
        }
        if (this.color.equals(((ColoredCards)card).getColor())){
                return true;
        }
        return false;
    }
}